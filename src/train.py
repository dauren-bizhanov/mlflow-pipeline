import os

import mlflow
import pandas as pd
import numpy as np
from sklearn.metrics import (
    f1_score,
    precision_score,
    recall_score,
)
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier


DATA_PATH = "data"
TRAIN = os.path.join(DATA_PATH, "train.csv")
TEST = os.path.join(DATA_PATH, "test.csv")


def load_dataframes(
    train_name: str, test_name: str
) -> tuple[pd.DataFrame, pd.DataFrame]:
    train = pd.read_csv(train_name)
    test = pd.read_csv(test_name)
    test.drop("id", inplace=True, axis=1)
    return train, test


def metrics(
    y_true: np.ndarray, y_pred: np.ndarray, average="weighted", test=False
) -> dict[str, float]:
    f1 = f1_score(y_true, y_pred, average=average)
    precision = precision_score(y_true, y_pred, average=average)
    recall = recall_score(y_true, y_pred, average=average)

    m_names = [
        name + "_test" if test else name + "_val"
        for name in ["f1", "precision", "recall"]
    ]
    return {k: v for k, v in zip(m_names, [f1, precision, recall])}


def train_model(model, name: str):

    x_train_scaled.to_csv("artifacts/x_train_scaled.csv", index=False)
    x_val_scaled.to_csv("artifacts/x_val_scaled.csv", index=False)

    mlflow.log_artifacts("artifacts")

    m = model()
    m.fit(x_train_scaled, y_train)
    val_scores = metrics(y_val, m.predict(x_val_scaled))
    mlflow.log_metrics(val_scores)
    mlflow.sklearn.log_model(
        m, name,
    )


if __name__ == "__main__":
    train_df, test_df = load_dataframes(TRAIN, TEST)
    target = "price_range"
    train_df, val_df = train_test_split(train_df, random_state=1234)
    x_train, y_train = train_df.iloc[:, :-1], train_df.iloc[:, -1]
    x_val, y_val = val_df.iloc[:, :-1], val_df.iloc[:, -1]

    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)
    x_val = scaler.transform(x_val)

    if "artifacts" not in os.listdir():
        os.mkdir("artifacts")

    x_train_scaled = pd.DataFrame(x_train)
    x_val_scaled = pd.DataFrame(x_val)

    with mlflow.start_run(run_name="LogisticRegression"):
        train_model(LogisticRegression, "log_reg")

    with mlflow.start_run(run_name="DecisionTree"):
        train_model(DecisionTreeClassifier, "tree")

    with mlflow.start_run(run_name="RandomForest"):
        train_model(RandomForestClassifier, "random_forest")
