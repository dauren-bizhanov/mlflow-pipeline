# MLFlow Workflow

The main goal of the project is to learn MLFlow concepts and to get used to its api. The [data](https://www.kaggle.com/datasets/iabhishekofficial/mobile-price-classification)
is used in the project comes from kaggle and the ML task is multiclass classification.

## Run

```shell
pip install -r requirements.txt
python src/train.py
mlflow ui
mlflow models serve -m <mlflow/path-to-model> -p <port name>
```

When run the last command MLFlow creates conda environment and isntalls all the dependencies. After the first run MLFlow creates a directory with the following structure:  
![img](img/mlflow.png)

It saves every experiment in a separate directory with logged metrics, artifacts (images, models, etc.).

## Resources
[Official MLFlow docs](https://mlflow.org/docs/latest/index.html)  
[Great PyData talk](https://www.youtube.com/watch?v=WbicniUy_u0&t=3941&ab_channel=PyData)